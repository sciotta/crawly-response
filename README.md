# crawly-response [![Build Status](https://gitlab.com/thiagog3/crawly-response/badges/master/build.svg)](https://gitlab.com/thiagog3/crawly-response/pipelines)



## Getting Started

#### 1. Setting Up environment

Clone this repository and install [nodeJS](http://nodejs.org/downloads/)!


#### 2. Download and install dependencies

Use this commands upon your cloned folder:

```
npm i
```

#### You are almost ready!

Use the folowing steps to run locally or test your applicattion:

#### Execute and shows result number

```
npm start
```

#### Test application

```
npm run test
```
